use near_sdk::borsh::{self, BorshDeserialize, BorshSerialize};
use near_sdk::log;
use near_sdk::serde::{Deserialize, Serialize};
use near_sdk::{env, near_bindgen, AccountId};
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Clone, BorshDeserialize, BorshSerialize)]
pub struct Votes {
    pub votes: Vec<AccountId>,
}

#[derive(Serialize, Deserialize)]
#[serde(crate = "near_sdk::serde")]
pub struct JsonToken {
    pub votes: HashMap<String, Votes>,
    pub winner: String,
}

#[near_bindgen]
#[derive(Serialize, Deserialize, BorshDeserialize, BorshSerialize)]
pub struct Voting {
    candidate_votes: HashMap<String, Votes>,
    is_poll_open: bool,
    vote_started: u64,
}
impl Default for Voting {
    fn default() -> Self {
        let mapped_candidates = HashMap::new();
        Self {
            is_poll_open: true,
            candidate_votes: mapped_candidates,
            vote_started: env::block_timestamp(),
        }
    }
}
#[near_bindgen]
impl Voting {
    pub fn add_candidates(&mut self, candidates: Vec<String>) {
        log!("{}", self.is_poll_open);
        if self.is_poll_open != true {
            env::panic(b"No active polling!");
        }
        for candidate in candidates.iter() {
            self.candidate_votes
                .insert(candidate.clone(), Votes { votes: vec![] });
        }
    }
    pub fn get_candidates(&self) -> Vec<String> {
        if self.is_poll_open != true {
            env::panic(b"No active polling!");
        }
        let candidates = self.candidate_votes.keys().cloned().collect();
        candidates
    }
    pub fn vote(&mut self, candidate: String) -> Votes {
        if !self.is_poll_open {
            env::panic(b"No active polling!");
        }
        let candidate_votes = self.candidate_votes.clone();
        let votes = candidate_votes.get(&candidate.clone());
        if votes.is_some() {
            log!("There is something");
            if votes
                .iter()
                .any(|&i| i.votes.contains(&env::signer_account_id()))
            {
                env::panic(b"You have already voted!");
            } else {
                self.candidate_votes.insert(
                    candidate.clone(),
                    Votes {
                        votes: vec![env::signer_account_id()],
                    },
                );
            }
        } else {
            log!("There is nothing");
        }
        self.candidate_votes.get(&candidate).unwrap().clone()
    }
    pub fn get_stats(&self) -> Option<JsonToken> {
        if !self.is_poll_open {
            env::panic(b"No active polling!");
        }
        let candidate_votes = self.candidate_votes.clone();
        let win = candidate_votes
            .iter()
            .max_by(|a, b| a.1.votes.len().cmp(&b.1.votes.len()))
            .map(|(k, _v)| k);
        log!("{:?}", win.unwrap());
        Some(JsonToken {
            votes: candidate_votes.clone(),
            winner: win.unwrap().clone(),
        })
    }
    pub fn get_results(&self) -> Option<JsonToken> {
        if self.is_poll_open {
            env::panic(b"Polling is active!");
        }
        let candidate_votes = self.candidate_votes.clone();
        let win = candidate_votes
            .iter()
            .max_by(|a, b| a.1.votes.len().cmp(&b.1.votes.len()))
            .map(|(k, _v)| k);
        log!("{:?}", win.unwrap());
        Some(JsonToken {
            votes: candidate_votes.clone(),
            winner: win.unwrap().clone(),
        })
    }
    pub fn end_poll(&mut self) {
        if !self.is_poll_open {
            env::panic(b"Polling is ended!");
        }
        self.is_poll_open = false;
    }
    pub fn start_poll(&mut self) {
        if self.is_poll_open {
            env::panic(b"Polling has already started!");
        }
        self.candidate_votes = HashMap::new();
        self.is_poll_open = true;
        self.vote_started = env::block_timestamp();
    }
    pub fn get_poll_status(&mut self) -> bool {
        self.is_poll_open
    }
}
