# Welcome to Voting Contract!
This is a voting contract build in **NEAR protocol**.
## Prerequisite
**Nodejs** and **Rust** must be installed in your machine.
# How to build contract?
## Step 1 :
`yarn install` or `npm install`

## Step 2 :
`yarn build` or `npm run build`




# Contract Calls

`near call $CONTRACT_ID  start_poll --accountId=$ACCOUNT_ID`

This method is to start the poll. Without calling this method, you can not call any other methods available.

`near call $CONTRACT_ID  add_candidates '{"candidates" :["Candidate 1", "Candidate 2"]}' --accountId=$ACCOUNT_ID`

This method is to add candidates to the current poll.

`near call $CONTRACT_ID  get_candidates --accountId=$ACCOUNT_ID`

This method returns all candidates. 

`near call $CONTRACT_ID  vote '{"candidate" :"Candidate 1"}' --accountId=$ACCOUNT_ID`

This method is to vote for a valid candidate. 

`near call $CONTRACT_ID  get_stats --accountId=$ACCOUNT_ID`

This method returns some stats and current leading candidate. 

`near call $CONTRACT_ID  end_poll --accountId=$ACCOUNT_ID`

This method ends the on going poll. 

`near call $CONTRACT_ID  get_results --accountId=$ACCOUNT_ID`

This method return the stats and winner of the previous poll. 
